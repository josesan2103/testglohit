package com.example.testhiglob.data.models

import com.google.gson.annotations.SerializedName

/**
 * @name MovieRatedResponse.kt
 * @description: MovieRatedResponse
 * @author Jose Sanchez Rosales (josesanchezutl@gmail.com)
 * @version: 1.0
 * @since 12/05/2021
 */
data class MovieRatedResponse(
    @SerializedName("id") val id: String,
    @SerializedName("title") val title: String,
    @SerializedName("body") val body: String,
    @SerializedName("duracion") val duracion: String,
    @SerializedName("image") val image: String
)