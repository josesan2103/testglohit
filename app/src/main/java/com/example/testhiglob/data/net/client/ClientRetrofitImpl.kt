package com.example.testhiglob.data.net.client

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

/**
 * @name ClientRetrofitImpl.kt
 * @description: Creacion inicial de archivo
 * @version: 1.0
 * @author Jose sanchez rosales (josesanchezutl@gmail.com)
 * @since 12/05/2021
 */
class ClientRetrofitImpl:IClientRetrofit {
    companion object{
        /**
         * val for retrofit
         */
        var  retrofit:Retrofit?=null

        /**
         * val for instance
         */
        var instance : ClientRetrofitImpl ? = null

        /**
         * function for get instance
         * @return return
         */
        fun getInstance():IClientRetrofit{
            if (instance==null){
                instance= ClientRetrofitImpl()
            }
            return instance!!
        }
    }

    /**
     * function for get client
     * @return retrofit
     */
    override fun getClient(): Retrofit {
        if (retrofit==null){
            retrofit= Retrofit.Builder()
                .baseUrl("https://api.themoviedb.org/")
                .client(getOkHttpClient())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
        }
        return retrofit!!
    }
    /**
     * Method that returns the okHttpClient instance
     *
     * OkHttp performs best when you create a single {@code OkHttpClient} instance and reuse it for
     * all of your HTTP calls. This is because each client holds its own connection pool and thread
     * pools. Reusing connections and threads reduces latency and saves memory. Conversely, creating a
     * client for each request wastes resources on idle pools.
     *
     * @return OkHttpClient
     */
    private fun getOkHttpClient(): OkHttpClient {
        val okHttpClient: OkHttpClient
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        /**
         * Create new instance of OkHttpClient
         */
        okHttpClient = OkHttpClient().newBuilder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .addInterceptor(loggingInterceptor)
            .build()
        return okHttpClient
    }

}