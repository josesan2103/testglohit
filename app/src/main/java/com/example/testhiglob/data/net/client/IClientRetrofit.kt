package com.example.testhiglob.data.net.client

import retrofit2.Retrofit

/**
 * @name IRetrofitRepository.kt
 * @description: Creacion inicial de archivo
 * @version: 1.0
 * @author Jose sanchez rosales (josesanchezutl@gmail.com)
 * @since 12/05/2021
 */
interface IClientRetrofit {
    /**
     * function for get the client
     */
    fun getClient(): Retrofit
}