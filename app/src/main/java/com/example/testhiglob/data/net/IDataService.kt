package com.example.testhiglob.data.net

import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url

/**
 * @name IRetrofitRepository.kt
 * @description: Creacion inicial de archivo
 * @version: 1.0
 * @author Jose sanchez rosales (josesanchezutl@gmail.com)
 * @since 12/05/2021
 */
interface IDataService {
    /**
     * function for get data
     */
    @GET
    fun getData(@Url url: String): Observable<Response<String>>
}