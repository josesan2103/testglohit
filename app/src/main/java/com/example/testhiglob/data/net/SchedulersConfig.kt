package com.example.testhiglob.data.net

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * @name IRetrofitRepository.kt
 * @description: Creacion inicial de archivo
 * @version: 1.0
 * @author Jose sanchez rosales (josesanchezutl@gmail.com)
 * @since 12/05/2021
 */
class SchedulersConfig : ISchedulers {
    /**
     * Method to provide main scheduler
     *
     * @return scheduler instance
     */
    override fun getMainScheduler(): Scheduler = AndroidSchedulers.mainThread()

    /**
     * Method to provide io scheduler
     *
     * @return scheduler instance
     */
    override fun getIoScheduler(): Scheduler = Schedulers.io()

    /**
     * Method to provide computation scheduler
     *
     * @return scheduler instance
     */
    override fun getComputationScheduler(): Scheduler = Schedulers.computation()

    /**
     * Method to provide trampoline scheduler
     *
     * @return scheduler instance
     */
    override fun getTrampolineScheduler(): Scheduler = Schedulers.trampoline()
}