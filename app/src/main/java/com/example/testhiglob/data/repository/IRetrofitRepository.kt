package com.example.testhiglob.data.repository

import com.example.testhiglob.data.net.IRetrofitGeneric

/**
 * @name IRetrofitRepository.kt
 * @description: Creacion inicial de archivo
 * @version: 1.0
 * @author Jose sanchez rosales (josesanchezutl@gmail.com)
 * @since 12/05/2021
 */
interface IRetrofitRepository {

    /**
     * function that init all the dependency
     */
    fun init(): IRetrofitRepository

    /**
     * function for set on success
     * @param successResponse any
     * @return instance
     */
    fun setOnSuccess(successResponse: (Any) -> Unit): IRetrofitRepository

    /**
     * function for set on error
     * @param errorResponse String
     * @return instance
     */
    fun setOnError(errorResponse: (Any) -> Unit): IRetrofitRepository

    /**
     * function that get the date
     */
    fun getInstanceNetwork(): IRetrofitGeneric
}