package com.example.testhiglob.data.net

import com.example.testhiglob.data.listener.IResponseListener


/**
 * @name IRetrofitRepository.kt
 * @description: Creacion inicial de archivo
 * @version: 1.0
 * @author Jose sanchez rosales (josesanchezutl@gmail.com)
 * @since 12/05/2021
 */
interface IRetrofitGeneric {
    /**
     * function for init the retrofit
     */
    fun init(): IRetrofitGeneric

    /**
     * function for set listener
     */
    fun setListener(listener: IResponseListener): IRetrofitGeneric

    /**
     * function for set interface
     */
    fun getData(url: String)

    /**
     * method for execute retrofit
     */
    fun execute(): IRetrofitGeneric
}