package com.example.testhiglob.data.net

import io.reactivex.Scheduler

/**
 * @name IRetrofitRepository.kt
 * @description: Creacion inicial de archivo
 * @version: 1.0
 * @author Jose sanchez rosales (josesanchezutl@gmail.com)
 * @since 12/05/2021
 */
interface ISchedulers {
    /**
     * Method to provide main scheduler
     *
     * @return scheduler instance
     */
    fun getMainScheduler(): Scheduler

    /**
     * Method to provide io scheduler
     *
     * @return scheduler instance
     */
    fun getIoScheduler(): Scheduler

    /**
     * Method to provide computation scheduler
     *
     * @return scheduler instance
     */
    fun getComputationScheduler(): Scheduler

    /**
     * Method to provide trampoline scheduler
     *
     * @return scheduler instance
     */
    fun getTrampolineScheduler(): Scheduler
}