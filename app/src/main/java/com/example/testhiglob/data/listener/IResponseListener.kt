package com.example.testhiglob.data.listener

/**
 * @name IRetrofitRepository.kt
 * @description: Creacion inicial de archivo
 * @version: 1.0
 * @author Jose sanchez rosales (josesanchezutl@gmail.com)
 * @since 12/05/2021
 */
interface IResponseListener {
    /**
     * on success
     * @param success object
     */
    fun onSuccess(success: Any?)

    /**
     * function on error
     * @param error string
     */
    fun onError(error: String)
}