package com.example.testhiglob.data.enum

/**
 * @name IRetrofitRepository.kt
 * @description: Creacion inicial de archivo
 * @version: 1.0
 * @author Jose sanchez rosales (josesanchezutl@gmail.com)
 * @since 12/05/2021
 */
enum class ErrorEnum(val value: String) {
    DEFAULT("Ocurrio un error, por favor intenta mas tarde"),
    TIME_OUT("No se logro obtener una respuesta, intenta mas tarde"),
    SERVER("Ocurrio un error en la consulta, por favor intenta mas tarde")
}