package com.example.testhiglob.data.net

import android.annotation.SuppressLint
import com.example.testhiglob.data.enum.ErrorEnum
import com.example.testhiglob.data.listener.IResponseListener
import com.example.testhiglob.data.net.client.ClientRetrofitImpl
import io.reactivex.functions.Consumer
import retrofit2.Response
import retrofit2.Retrofit
import java.net.SocketTimeoutException

/**
 * @name RetrofitGenericImpl.kt
 * @description: Creacion inicial de archivo
 * @version: 1.0
 * @author Jose sanchez rosales (josesanchezutl@gmail.com)
 * @since 12/05/2021
 */
class RetrofitGenericImpl : IRetrofitGeneric {
    private lateinit var listener: IResponseListener
    private lateinit var retrofit: Retrofit
    private var dataService: IDataService? = null
    private lateinit var schedulers: ISchedulers

    /**
     * Instance of consumer on success
     */
    private var onSuccess: Consumer<Response<String>>? = null

    /**
     * Instance of consumer on ERROR
     */
    private var onError: Consumer<Throwable>? = null

    /**
     * function for init
     */
    override fun init(): IRetrofitGeneric {
        val client = ClientRetrofitImpl.getInstance()
        retrofit = client.getClient()
        this.schedulers = SchedulersConfig()
        onSuccess = Consumer { manageServerResponse(it) }
        onError = Consumer { manageErrorResponse(it) }
        return this
    }

    /**
     * function for manage error
     * @param error trow
     */
    private fun manageErrorResponse(error: Throwable?) {
        if (error is SocketTimeoutException) {
            listener.onError(ErrorEnum.TIME_OUT.value)
        } else {
            listener.onError(ErrorEnum.SERVER.value)
        }

    }

    /**
     * function for manage server
     * @param response response
     */
    private fun manageServerResponse(response: Response<String>) {
        if (response.body() == null) {
            listener.onError(ErrorEnum.DEFAULT.value)
        } else {
            listener.onSuccess(response.body())
        }
    }

    /**
     * function for listener
     * @param listener listener
     * @return instance
     */
    override fun setListener(listener: IResponseListener): IRetrofitGeneric {
        this.listener = listener
        return this
    }

    /**
     * function for execute network
     * @return instance
     */
    override fun execute(): IRetrofitGeneric {
        dataService = retrofit.create(IDataService::class.java)
        return this
    }

    /**
     * method the interface for call service
     * @param url string
     */
    @SuppressLint("CheckResult")
    override fun getData(url: String) {
        dataService!!
            .getData(url)
            .subscribeOn(schedulers.getIoScheduler())
            .observeOn(schedulers.getMainScheduler())
            .subscribe(onSuccess!!, onError!!)
    }
}