package com.example.testhiglob.data.repository

import com.example.testhiglob.data.enum.ErrorEnum
import com.example.testhiglob.data.listener.IResponseListener
import com.example.testhiglob.data.net.IRetrofitGeneric
import com.example.testhiglob.data.net.RetrofitGenericImpl


/**
 * @name IRetrofitRepository.kt
 * @description: Creacion inicial de archivo
 * @version: 1.0
 * @author Jose sanchez rosales (josesanchezutl@gmail.com)
 * @since 12/05/2021
 */
class RetrofitRepositoryImpl : IRetrofitRepository, IResponseListener {
    /**
     * val for error
     */
    private lateinit var errorResponse: (Any) -> Unit

    /**
     * val for success
     */
    private lateinit var successResponse: (Any) -> Unit

    /**
     * val for retrofit
     */
    private lateinit var retrofitGeneric: IRetrofitGeneric

    /**
     * function init for repository
     * @return instance
     */
    override fun init(): IRetrofitRepository {
        this.retrofitGeneric = RetrofitGenericImpl()
        return this
    }

    /**
     * function on success
     * @param successResponse unit
     * @return instance
     */
    override fun setOnSuccess(successResponse: (Any) -> Unit): IRetrofitRepository {
        this.successResponse = successResponse
        return this
    }

    /**
     * function on Error
     * @param errorResponse unit
     * @return instance
     */
    override fun setOnError(errorResponse: (Any) -> Unit): IRetrofitRepository {
        this.errorResponse = errorResponse
        return this
    }

    /**
     * method get data
     * @return instance
     */
    override fun getInstanceNetwork(): IRetrofitGeneric {
        return retrofitGeneric.init()
                .setListener(this)
                .execute()
    }

    /**
     * method on success
     */
    override fun onSuccess(success: Any?) {
        if (success == null) {
            errorResponse.invoke(ErrorEnum.DEFAULT.value)
        } else {
            successResponse.invoke(success)
        }
    }

    /**
     * method on error
     */
    override fun onError(error: String) {
        errorResponse.invoke(error)
    }
}