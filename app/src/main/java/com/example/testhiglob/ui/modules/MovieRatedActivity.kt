package com.example.testhiglob.ui.modules

import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.testhiglob.R
import com.example.testhiglob.data.models.MovieRatedResponse
import com.example.testhiglob.domain.GetMoviesRatedCaseImpl
import com.example.testhiglob.domain.IGetMovieRatedCase
import com.example.testhiglob.ui.modules.adapters.MovieRatedAdapter
import com.example.testhiglob.ui.modules.interfaces.IListenerAdapterMovie

/**
 * @name MovieRatedActivity.kt
 * @description: MovieRatedActivity
 * @author Jose Sanchez Rosales (josesanchezutl@gmail.com)
 * @version: 1.0
 * @since 12/05/2021
 */
class MovieRatedActivity : AppCompatActivity(), IListenerAdapterMovie {

    //lateinit var tvTitleMovieRated: TextView
    private lateinit var rvMovieRated: RecyclerView
//    val progressDialog = ProgressDialog(this@MovieRatedActivity)

    /**
     * val for case
     */
    private lateinit var getMovieRatedCase: IGetMovieRatedCase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_rated)

        val tvTitleMovieRated: TextView = findViewById(R.id.tvTitleMovieRated)
        rvMovieRated = findViewById<RecyclerView>(R.id.rvMovieRated)
        //tvTitleMovieRated.findViewById<TextView>(R.id.tvTitleMovieRated)
        //rvMovieRated.findViewById<RecyclerView>(R.id.rvMovieRated)

        tvTitleMovieRated.text = "MOVIE RATED"
        rvMovieRated.layoutManager = LinearLayoutManager(this)
//        progressDialog.setMessage("Cargando...")
//        progressDialog.setCancelable(false)

        getMovieRatedCase = GetMoviesRatedCaseImpl()
        getListMovies()
    }

    private fun getListMovies() {
        //progressDialog.show()
        getMovieRatedCase
            .init()
            .onSuccess {
                llenarAdapterMoviesRated(it)
            }.onError {
                showError(it)
            }.executeCase()
    }

    private fun showError(error: String) {
        //progressDialog.dismiss()
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
    }

    private fun llenarAdapterMoviesRated(listMoviesRated: List<MovieRatedResponse>) {
        //progressDialog.dismiss()
        rvMovieRated.adapter = MovieRatedAdapter(listMoviesRated, applicationContext, this)
    }

    override fun movieRatedSelected(movieSelected: MovieRatedResponse) {
        Toast.makeText(this, "Selecciono: ${movieSelected.title}", Toast.LENGTH_SHORT).show()
    }


}