package com.example.testhiglob.ui.modules.interfaces

import com.example.testhiglob.data.models.MovieRatedResponse

/**
 * @name IListenerAdapterMovie.kt
 * @description: IListenerAdapterMovie
 * @author Jose Sanchez Rosales (josesanchezutl@gmail.com)
 * @version: 1.0
 * @since 12/05/2021
 */
interface IListenerAdapterMovie {
    /**
     * fun for get movie selected
     */
    fun movieRatedSelected(movieSelected : MovieRatedResponse)
}