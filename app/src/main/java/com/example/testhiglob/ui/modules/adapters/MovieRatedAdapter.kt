package com.example.testhiglob.ui.modules.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.testhiglob.R
import com.example.testhiglob.data.models.MovieRatedResponse
import com.example.testhiglob.ui.modules.interfaces.IListenerAdapterMovie
import com.squareup.picasso.Picasso

/**
 * @name MovieRatedAdapter.kt
 * @description: MovieRatedAdapter
 * @author Jose Sanchez Rosales (josesanchezutl@gmail.com)
 * @version: 1.0
 * @since 12/05/2021
 */
class MovieRatedAdapter(
    private val listItemsMovieRatec: List<MovieRatedResponse>,
    private val context: Context,
    private val listener: IListenerAdapterMovie
) :
    RecyclerView.Adapter<MovieRatedAdapter.ViewHolder>() {

    /**
     * Called when RecyclerView needs a new {@link ViewHolder} of the given type to represent
     * an item.
     * <p>
     * This new ViewHolder should be constructed with a new View that can represent the items
     * of the given type. You can either create a new View manually or inflate it from an XML
     * layout file.
     * <p>
     * The new ViewHolder will be used to display items of the adapter using
     * {@link #onBindViewHolder(ViewHolder, int, List)}. Since it will be re-used to display
     * different items in the data set, it is a good idea to cache references to sub views of
     * the View to avoid unnecessary {@link View#findViewById(int)} calls.
     *
     * @param parent The ViewGroup into which the new View will be added after it is bound to
     *               an adapter position.
     * @param viewType The view type of the new View.
     *
     * @return A new ViewHolder that holds a View of the given view type.
     * @see #getItemViewType(int)
     * @see #onBindViewHolder(ViewHolder, int)
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutView: View =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_movierated_adapter, parent, false)
        return ViewHolder(layoutView)
    }

    /**
     * Called by RecyclerView to display the data at the specified position. This method should
     * update the contents of the {@link ViewHolder#itemView} to reflect the item at the given
     * position.
     * <p>
     * Note that unlike {@link android.widget.ListView}, RecyclerView will not call this method
     * again if the position of the item changes in the data set unless the item itself is
     * invalidated or the new position cannot be determined. For this reason, you should only
     * use the <code>position</code> parameter while acquiring the related data item inside
     * this method and should not keep a copy of it. If you need the position of an item later
     * on (e.g. in a click listener), use {@link ViewHolder#getAdapterPosition()} which will
     * have the updated adapter position.
     *
     * Override {@link #onBindViewHolder(ViewHolder, int, List)} instead if Adapter can
     * handle efficient partial bind.
     *
     * @param holder The ViewHolder which should be updated to represent the contents of the
     *        item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvTitleMovieRated.text = listItemsMovieRatec[position].title
        Picasso
            .with(context)
            .load("https:" + listItemsMovieRatec[position].image)
            .error(R.mipmap.ic_launcher)
            .into(holder.ivMovieRated)

        holder.tvDescriptionMovie.text = listItemsMovieRatec[position].body
        holder.tvDuracionMovie.text = listItemsMovieRatec[position].duracion

        holder.clContenedorMovieRated.setOnClickListener {
            listener.movieRatedSelected(listItemsMovieRatec[position])
        }

    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    override fun getItemCount(): Int = listItemsMovieRatec.size

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val clContenedorMovieRated: ConstraintLayout =
            view.findViewById(R.id.clContenedorMovieRated)
        val tvTitleMovieRated: TextView =
            view.findViewById(R.id.tvTitleMovieRated)
        val ivMovieRated: ImageView =
            view.findViewById(R.id.ivMovieRated)
        val tvDescriptionMovie: TextView =
            view.findViewById(R.id.tvDescriptionMovie)
        val tvDuracionMovie: TextView =
            view.findViewById(R.id.tvDuracionMovie)
    }

}