package com.example.testhiglob.domain

import com.example.testhiglob.data.models.MovieRatedResponse

/**
 * @name IGetMovieRatedCase.kt
 * @description: IGetMovieRatedCase
 * @author Jose Sanchez Rosales (josesanchezutl@gmail.com)
 * @version: 1.0
 * @since 12/05/2021
 */
interface IGetMovieRatedCase {
    /**
     * method for init the dependency
     * @return instance
     */
    fun init(): IGetMovieRatedCase


    /**
     * function on success
     * @param success method with action success
     * @return instance
     */
    fun onSuccess(success: (List<MovieRatedResponse>) -> Unit): IGetMovieRatedCase

    /**
     * function on error
     * @param error method with action error
     * @return instance
     */
    fun onError(error: (String) -> Unit): IGetMovieRatedCase

    /**
     * function for execute case
     */
    fun executeCase()
}