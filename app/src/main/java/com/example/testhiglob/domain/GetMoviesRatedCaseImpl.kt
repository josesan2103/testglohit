package com.example.testhiglob.domain


import com.example.testhiglob.data.models.MovieRatedResponse
import com.example.testhiglob.data.repository.IRetrofitRepository
import com.example.testhiglob.data.repository.RetrofitRepositoryImpl
import com.google.gson.Gson

/**
 * @name GetMoviesRatedCaseImpl.kt
 * @description: GetMoviesRatedCaseImpl
 * @author Jose Sanchez Rosales (josesanchezutl@gmail.com)
 * @version: 1.0
 * @since 12/05/2021
 */
class GetMoviesRatedCaseImpl : IGetMovieRatedCase {
    /**
     * val for error
     */
    private lateinit var error: (String) -> Unit

    /**
     * val for success
     */
    private lateinit var success: (List<MovieRatedResponse>) -> Unit

    /**
     * val for repository
     */
    private lateinit var repository: IRetrofitRepository

    /**
     * function init
     * @return instance
     */
    override fun init(): IGetMovieRatedCase {
        this.repository = RetrofitRepositoryImpl()
        return this
    }

    /**
     * method on success
     * @param success unit
     * @return instance
     */
    override fun onSuccess(success: (List<MovieRatedResponse>) -> Unit): IGetMovieRatedCase {
        this.success = success
        return this
    }

    /**
     * function on error
     * @param error unit
     * @return instance
     */
    override fun onError(error: (String) -> Unit): IGetMovieRatedCase {
        this.error = error
        return this
    }

    /**
     * function for execute case
     */
    override fun executeCase() {
        repository.init()
            .setOnError {
                error.invoke(it as String)
            }.setOnSuccess {
                validateResponse(it)
            }.getInstanceNetwork()
            .getData("account/lists")
    }

    /**
     * function for validate response
     */
    private fun validateResponse(response: Any) {
        val res = response as String
        val responseObject: List<MovieRatedResponse> =
            Gson().fromJson(res, Array<MovieRatedResponse>::class.java).toList()
        success.invoke(responseObject)
    }
}